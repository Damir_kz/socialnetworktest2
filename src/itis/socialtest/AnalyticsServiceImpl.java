package itis.socialtest;

import itis.socialtest.entities.Post;
import java.util.Optional;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AnalyticsServiceImpl implements AnalyticsService {
    @Override
    public List<Post> findPostsByDate(List<Post> posts, String date) {
        return posts.stream().filter(a -> a.getDate().equals(date))
                .collect(Collectors.toList());
    }


    @Override
    public String findMostPopularAuthorNickname(List<Post> posts) {
        Optional<Post> max = posts.stream()
                .max(Comparator.comparing(Post::getLikesCount));
        return String.valueOf(max.get().getAuthor().getNickname());
    }

    @Override
    public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
        return posts.stream()
                .anyMatch(a -> a.getContent().contains(searchString));
    }

    @Override
    public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick) {
        return posts.stream()
                .filter(a -> a.getAuthor().getNickname().equals(nick))
                .collect(Collectors.toList());
    }
}
