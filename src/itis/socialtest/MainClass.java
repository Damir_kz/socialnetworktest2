package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие операции:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {

        new MainClass().run("",     "");

        Path path = Path.of("C:\\Windows.old\\Users\\Damir\\IdeaProjects\\socialnetworktest2\\src\\itis\\socialtest\\resources\\Authors.csv");
        try {
            List<String> list = Files.readAllLines(path);
            List<Author> list1 = new ArrayList<>();
            for (String s : list) {
                String[] ar = s.split(",");
                list1.add(new Author(Long.parseLong(ar[0]), ar[1], ar[2]));
            }
            MainClass.givenToFile(list1);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void run(String postsSourcePath, String authorsSourcePath) {

    }


    public static void givenToFile(List<Author> ar) throws FileNotFoundException {
        File output = new File("output.csv");
        try (PrintWriter pw = new PrintWriter(output)) {
            ar.stream().map(Author::convertToCSV).forEach(pw::println);
        }
    }
}
